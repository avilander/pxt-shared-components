/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { PxtHttpService } from './pxt-http.service';
/**
 * @template T
 */
var RequestBaseService = /** @class */ (function () {
    function RequestBaseService(httpService) {
        this.httpService = httpService;
    }
    /**
     * @param {?} urlApi
     * @param {?=} model
     * @return {?}
     */
    RequestBaseService.prototype.load = /**
     * @param {?} urlApi
     * @param {?=} model
     * @return {?}
     */
    function (urlApi, model) {
        return this.httpService.doGet(urlApi);
    };
    ;
    /**
     * @param {?} urlApi
     * @param {?=} model
     * @return {?}
     */
    RequestBaseService.prototype.save = /**
     * @param {?} urlApi
     * @param {?=} model
     * @return {?}
     */
    function (urlApi, model) {
        return this.httpService.doPost(urlApi, model);
    };
    ;
    /**
     * @param {?} urlApi
     * @param {?=} model
     * @return {?}
     */
    RequestBaseService.prototype.delete = /**
     * @param {?} urlApi
     * @param {?=} model
     * @return {?}
     */
    function (urlApi, model) {
        return this.httpService.doDelete(urlApi, '');
    };
    ;
    RequestBaseService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    RequestBaseService.ctorParameters = function () { return [
        { type: PxtHttpService }
    ]; };
    return RequestBaseService;
}());
export { RequestBaseService };
if (false) {
    /** @type {?} */
    RequestBaseService.prototype.httpService;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVxdWVzdC1iYXNlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9weHQtc2hhcmVkLWNvbXBvbmVudHMvIiwic291cmNlcyI6WyJzcmMvYXBwL3NlcnZpY2VzL3B4dC1odHRwL3JlcXVlc3QtYmFzZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQzs7Ozs7SUFLbEQsNEJBQW9CLFdBQTJCO1FBQTNCLGdCQUFXLEdBQVgsV0FBVyxDQUFnQjtLQUM5Qzs7Ozs7O0lBQ0QsaUNBQUk7Ozs7O0lBQUosVUFBSyxNQUFNLEVBQUcsS0FBUztRQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7S0FDdkM7SUFBQSxDQUFDOzs7Ozs7SUFDRixpQ0FBSTs7Ozs7SUFBSixVQUFLLE1BQU0sRUFBRyxLQUFTO1FBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7S0FDL0M7SUFBQSxDQUFDOzs7Ozs7SUFDRixtQ0FBTTs7Ozs7SUFBTixVQUFPLE1BQU0sRUFBRyxLQUFTO1FBQ3ZCLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUcsRUFBRSxDQUFDLENBQUM7S0FDL0M7SUFBQSxDQUFDOztnQkFiSCxVQUFVOzs7O2dCQUZGLGNBQWM7OzZCQUR2Qjs7U0FJYSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQeHRIdHRwU2VydmljZSB9IGZyb20gJy4vcHh0LWh0dHAuc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBSZXF1ZXN0QmFzZVNlcnZpY2UgPFQ+IHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHBTZXJ2aWNlOiBQeHRIdHRwU2VydmljZSkge1xuICB9XG4gIGxvYWQodXJsQXBpICwgbW9kZWw/OiBUKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5odHRwU2VydmljZS5kb0dldCh1cmxBcGkpO1xuICB9O1xuICBzYXZlKHVybEFwaSAsIG1vZGVsPzogVCk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cFNlcnZpY2UuZG9Qb3N0KHVybEFwaSwgbW9kZWwpO1xuICB9O1xuICBkZWxldGUodXJsQXBpICwgbW9kZWw/OiBUKSA6YW55e1xuICAgIHJldHVybiB0aGlzLmh0dHBTZXJ2aWNlLmRvRGVsZXRlKHVybEFwaSAsICcnKTtcbiAgfTtcbn1cbiJdfQ==